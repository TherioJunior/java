import java.util.Scanner;

public class SearchForAValue {
    /* Datatype must be int, as the assignment requests that the return value is the index position in which TargetValue was found */
    /* and that if it wasn't found, that the return value is -1. */
    public static int SearchForAValueInArray(int TargetValue, int[] Array) {
        /* Initialize an integer which holds the index the TargetValue was found in. The default value is -1, so that if the */
        /* TargetValue isn't found, we automatically return -1 as the assignment requests. */
        int FoundAtIndex = -1;

        /* Loop through the whole input parameter Array */
        for (int i = 0; i < Array.length; i++) {
            /* If the input parameter TargetValue is equal to the value inside the Array input parameter at the index */
            /* of the current loop iteration, the FoundAtIndex variable gets the current loop iteration assigned as value. */
            if (TargetValue == Array[i]) {
                FoundAtIndex = i;
            }
        }

        /* Return the FoundAtIndex variable. Possible values are -1 (the default value) if the TargetValue wasn't found */
        /* or an index position inside the Array if the TargetValue was found. */
        return FoundAtIndex;
    }

    public static void main(String[] args) {
        /* Initialize a new array which holds some random numbers. */
        int[] ArrayToSearch = { 3912, 128, 9312, 3000, 3059, 2391, 9999 };
        /* Initialize a new integer which will hold the return value of the SearchForAValueInArray method. */
        int FoundAtIndex;
        /* Initialize a new integer which will have its value assigned via a scanner, so the user can specify a TargetValue. */
        int TargetValue;

        /* Initialize a new Scanner which will be used to get the users input, and prompt the user afterward to input a number. */
        /* Last step is to read what the user said. */
        Scanner scan = new Scanner(System.in);
        System.out.print("Which value do you want to search the array for? ");
        TargetValue = scan.nextInt();

        /* Call the SearchForAValueInArray method with the just scanned user input and the previously created array. */
        FoundAtIndex = SearchForAValueInArray(TargetValue, ArrayToSearch);
        /* Print the results of the search */
        if (FoundAtIndex == -1) { /* TargetValue wasn't found in ArrayToSearch */
            System.out.println("Target value of " + TargetValue + " wasn't found in the array.");
        } else { /* TargetValue was found in ArrayToSearch */
            System.out.println("Target value of " + TargetValue + " was found at index " + FoundAtIndex);
        }
    }
}
