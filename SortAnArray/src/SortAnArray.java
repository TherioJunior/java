public class SortAnArray {
    public static void Bubblesort(int[] Array) {
        /* Create a loop which runs from the first element to the last element. */
        for (int i = 0; i < Array.length; i++) {
            /* Create a loop which runs from the first element to the last unsorted element, which compares the current iterations element */
            /* with the next element in the Array, and if one is bigger than the other, it swaps them. */
            for (int j = 0; j < Array.length - i - 1; j++) {
                /* Compare the current iterations value with the next value. If the current one is bigger than the next, this if statement is true */
                if (Array[j] > Array[j + 1]) {
                    /* Create a temporary variable which backs up the value of the current loop iteration. */
                    int Temp = Array[j];

                    /* Swap the values of the current iteration with the next iterations value. */
                    Array[j] = Array[j + 1];
                    /* Swap the next iteration value with the previously created backup value of the current iteration. */
                    Array[j + 1] = Temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        /* Initialize a new array which holds random numbers from big to small in a random order, so we can see the sorting result well. */
        int[] Array = { 5, 2, 8, 300, 3902, 20, 10000, 230, 500 };

        /* Call the Bubblesort method to sort the input array. */
        Bubblesort(Array);

        /* Print out the now sorted array, after our Bubblesort method has finished. */
        System.out.print("Sorted array: ");
        for (int i = 0; i < Array.length; i++) {
            System.out.print(Array[i] + " ");
        }
    }
}
