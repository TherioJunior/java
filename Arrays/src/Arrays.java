import java.util.Scanner;

public class Arrays {
    /* Doesn't need a return value, as the input parameter automatically populates a array created outside the scope of this method */
    /* which means that the datatype can be void. */
    public static void InputArray(int[] array, int length) {
        /* Initialize a new scanner to be able to read what the user said. */
        Scanner scan = new Scanner(System.in);

        for (int i = 0; i < length - 1; i++) {
            /* Prompt the user to introduce the value of a new element in the array. */
            /* I added '1' to the iteration count in this statement, to make sure the last printed number matches the input number by the user */
            System.out.print("Please input the value of index " + (i + 1) + " of the array: ");

            /* Read the input the user just gave, and save it in a integer variable. */
            int ScannedValue = scan.nextInt();
            /* Populate the array at the given loop iteration position with the just scanned value. */
            array[i] = ScannedValue;
        }
    }

    /* Datatype of the method must be int, since the assignment of exercise 8 says the output method should have the number of integers */
    /* actually input into the method as the method's return value. */
    public static int OutputArray(int[] array) {
        /* Loop through the array to output all numbers in it */
        for (int i = 0; i < array.length; i++) {
            /* Access the integer inside the array at the current loop iteration count and print it. */
            /* Will start at 0, because the first index in an array is 0. */
            /* I added '1' to the iteration count in this statement, to make sure the last printed number matches the input number by the user */
            System.out.println("Number " + (i + 1) + " of the array is: " + array[i]);
        }

        /* Return the number of integers actually input. */
        return array.length;
    }

    public static void main(String[] args) {
        /* Initialize a new scanner so we can get the target length from the user */
        Scanner scanner = new Scanner(System.in);

        /* Prompt the user for his target length and then read the input afterwards. */
        System.out.print("Enter the number of elements in the array: ");
        int length = scanner.nextInt();

        /* Initialize a new empty array with a length of 4 which we will populate using the InputArray method. */
        int[] EmptyArray = new int[length];

        /* Call the InputArray method with the length given by the user, so the user will be prompted to give the different numbers. */
        /* The '+ 1' has to be here because the assignment requests the loop to run from index 0 to index length -1. */
        /* Without this, the last index of the array when printing it out via the OutputArray method, will always be 0 based on my tests. */
        InputArray(EmptyArray, length + 1);

        /* Create a new integer which will hold the number of integers input. */
        int InputIntegers;

        /* Call the OutputArray method with the just populated array and assign its return value to the previously created variable */
        InputIntegers = OutputArray(EmptyArray);

        /* Simply print the return value of OutputArray */
        System.out.println("The number of integers inside the array is " + InputIntegers);
    }
}
