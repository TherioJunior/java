public class TheGreatestCommonDivisor {
    /* Calculates the greatest common divisor of two integers. */
    /* The greatest common divisor is the largest number that can divide both numbers without leaving a remainder. */
    public static int gcd(int a, int b) {
        if (b == 0) { /* If the second number is 0, the greatest common divisor is the first number, so return a. */
            return a;
        } else { /* Run this function repeatedly to calculate the greatest common divisor of b and the remainder of a divided by b. */
            return gcd(b, a % b);
        }
    }

    public static void main(String[] args) {
        // Test the gcd method with different pairs of numbers
        System.out.println("GCD of 54 and 24 is: " + gcd(54, 24)); // Expected: 6
        System.out.println("GCD of 111 and 259 is: " + gcd(111, 259)); // Expected: 37
        System.out.println("GCD of 35 and 10 is: " + gcd(35, 10)); // Expected: 5
    }
}
